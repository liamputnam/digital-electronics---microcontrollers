/*
MIT License

Copyright (c) 2019 Liam Putnam

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.


This code is supplimentary to our report, but if you, Dr. Squier would like to 
use it, it is licensed by the MIT License. Have fun!

- Liam Putnam, Nick Jarmusz, and Alan Durick. Dec 2019.
*/
#include <Wire.h>
#include <Adafruit_AMG88xx.h>

#define TRIG_PIN 13
#define ECHO_PIN 12
#define LED1_PIN 8 // blue
#define LED2_PIN 9 // green
#define LED3_PIN 10 // red
#define MOTOR_PIN 7
Adafruit_AMG88xx amg;

float pixels[AMG88xx_PIXEL_ARRAY_SIZE];

static void alarm(float, float, float);
static long sonar();

void setup()
{
    Serial.begin(9600);
    Serial.println(F("AMG88xx pixels"));

    bool status = amg.begin();
    if (!status) 
    {
        Serial.println("Could not find a valid AMG88xx sensor, check wiring!");
        while (1);
    }

    delay(100); // let sensor boot up

   
    // sonar setup
    pinMode(TRIG_PIN, OUTPUT);
    pinMode(ECHO_PIN, INPUT);
    
    // LED setup
    pinMode(LED1_PIN, OUTPUT);
    pinMode(LED2_PIN, OUTPUT);
    pinMode(LED3_PIN, OUTPUT);

    //Motor
    pinMode(MOTOR_PIN, OUTPUT);
}


void loop() 
{
    int print = 0;
start:
    long distance = sonar();
    if (distance < 40 && distance > 10) {
        digitalWrite(MOTOR_PIN, LOW);
        alarm(1);
        Serial.print(distance);
        Serial.println(" cm");
    } 
    else if(distance >= 40)
    {
        Serial.print(distance);
        Serial.println(" cm");
        alarm(2);
        goto start;
    }

     //read all the pixels
    amg.readPixels(pixels);

    Serial.print("[");
    for(unsigned int i = 1; i <= AMG88xx_PIXEL_ARRAY_SIZE; ++i)
    {
        Serial.print(pixels[i - 1]);
        Serial.print(", ");
        if( i%8 == 0 ) Serial.println();
        
        if (pixels[i - 1] >= 25.0) 
        {
            alarm(0);   
            digitalWrite(MOTOR_PIN, HIGH);      
        } 
    }


    Serial.println("]");
    Serial.println();
  
    
    //delay a second
    delay(5000);
}

void alarm(unsigned int flag) 
{
    if(flag == 0)
    {
        // red
        digitalWrite(LED1_PIN,HIGH); // When the Red condition is met, the Green LED should turn off
        digitalWrite(LED2_PIN,HIGH);
        digitalWrite(LED3_PIN, LOW);
    }
    else if(flag == 1)
    {
        // green
        digitalWrite(LED1_PIN, LOW);
        digitalWrite(LED2_PIN, HIGH);
        digitalWrite(LED3_PIN, HIGH);
    }
    else if(flag == 2)
    {
        // white
        digitalWrite(LED1_PIN, LOW);
        digitalWrite(LED2_PIN, LOW);
        digitalWrite(LED3_PIN, LOW);
      
    }
}

long sonar() {
    long duration = 0, distance = 0;
    digitalWrite(TRIG_PIN, LOW);  // Added this line
    delayMicroseconds(2); // Added this line
    digitalWrite(TRIG_PIN, HIGH);
    //  delayMicroseconds(1000); - Removed this line
    delayMicroseconds(10); // Added this line
    digitalWrite(TRIG_PIN, LOW);
    duration = pulseIn(ECHO_PIN, HIGH);
    distance = (duration / 2) / 29.1;
    
    return distance;
}
